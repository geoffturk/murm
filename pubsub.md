# Node Synchronization Across Services

The current implementation of the protocol is running:
  - Index on Kubernetes @ Digital Ocean
  - MPG and Aggregator on two separate Firebase instances @ Google
  - Search functionality is provided by Algolia

When a user deletes a node profile from (or changes a profile in) the Index, it is not removed from (or updated in) the Aggregator nor from (in) Algolia's search results.

## Current State

### User interacting directly with Index

```mermaid
sequenceDiagram

  participant u as Profile Owner
  participant n as Node
  participant i as Index
  participant m as MPG
  participant ag as Aggregator
  participant al as Algolia

  Note over i: 1, 2, 3...
  Note over ag, al: 1, 2, 3...
  u->>+n: Remove profile
  n->>-u: Profile removed
  u->>+i: Remove profile 2
  i->>+n: Check for profile 2
  n->>-i: Profile not found
  i->>i: Remove profile 2 record
  Note over i: 1, 3...
  Note over ag, al: 1, 2, 3...
  i->>-u: Profile 2 removed
```

### User interacting with Index through MPG

```mermaid
sequenceDiagram

  participant u as Profile Owner
  participant n as Node
  participant i as Index
  participant m as MPG
  participant ag as Aggregator
  participant al as Algolia

  Note over i, m: 1, 2, 3...
  Note over ag, al: 1, 2, 3...
  u->>+m: Remove profile
  m->>m: Profile removed
  m->>+i: Remove profile
  i->>+m: Check for profile
  m->>-i: Profile not found
  i->>i: Remove profile record
  i->>-m: Profile removed
  Note over i, m: 1, 3...
  Note over ag, al: 1, 2, 3...
  m->>-u: Profile removed
```

Given the current setup, the only way to get the Aggregator (and Algolia) synchronized with the Index is for the Aggregator to constantly poll the Index for all of the profile records associated with the schema(s) it wants to display, and then remove any profiles that are no longer found through the Index (and of course add any new ones retrieved). This approach is not ideal because the Aggregator will need to frequently spam the Index with requests just to see if anything has changed for any of the schemas it wants to track.

## Preferred State

A better option would be a to use a publish-subscribe (pubsub) pattern so that the Aggregator can subscribe to updates for specific schemas and the Index publishes those updates to all interested parties (the Aggregator being one of them but any number of Aggregators or other services could subscribe).

Not only will updates and removals of of profile records be published to all interested parties, but new profile records being added will also be published.

### User interacting directly with Index

```mermaid
sequenceDiagram

  participant u as Profile Owner
  participant n as Node
  participant i as Index
  participant m as MPG
  participant ag as Aggregator
  participant al as Algolia

  Note over i: 1, 2, 3...
  Note over ag, al: 1, 2, 3...
  ag-->>i: Subscribe to schema updates
  u->>+n: Remove profile
  n->>-u: Profile removed
  u->>+i: Remove profile 2
  i->>+n: Check for profile 2
  n->>-i: Profile not found
  i->>i: Remove profile 2 record
  Note over i: 1, 3...
  i-->>ag: Profile 2 removed
  i->>-u: Profile 2 removed
  ag->>ag: Remove profile 2
  ag->>+al: Remove profile 2 record
  al->>al: Remove profile 2
  al->>-ag: Profile 2 removed
  Note over ag, al: 1, 3...
```

### User interacting with Index through MPG

```mermaid
sequenceDiagram

  participant u as Profile Owner
  participant n as Node
  participant i as Index
  participant m as MPG
  participant ag as Aggregator
  participant al as Algolia

  Note over i, m: 1, 2, 3...
  Note over ag, al: 1, 2, 3...
  ag-->>i: Subscribe to schema updates
  u->>+m: Remove profile
  m->>m: Profile removed
  m->>+i: Remove profile
  i->>+m: Check for profile
  m->>-i: Profile not found
  i->>i: Remove profile record
  i->>-m: Profile removed
  Note over i, m: 1, 3...
  i-->>ag: Profile 2 removed
  m->>-u: Profile removed
  ag->>ag: Remove profile 2
  ag->>+al: Remove profile 2 record
  al->>al: Remove profile 2
  al->>-ag: Profile 2 removed
  Note over ag, al: 1, 3...
```

## To Do

- Discuss pubsub approach with Adam, Max, Bryan
- Choose a pubsub framework to run in the K8s cluster with the Index (checkout [Dapr](https://awesomeopensource.com/project/dapr/dapr) as a starting point)
- Implement pubsub in Murm K8s cluster
- Setup Aggregator to subscribe to events from the Index
